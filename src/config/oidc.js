export const oidcSettings = {
    authority: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_UoUR9ev8F/.well-known/openid-configuration',
    clientId: '53ojvflvqeav5atnfine5ebkg6',
    redirectUri: window.location.origin + '/oidc-callback',
    responseType: 'code',
    scope: 'openid profile phone email',
    silentRedirectUri: window.location.origin + '/oidc-silent-renew.html',
    automaticSilentRenew: true/*,
    metadata: {
        end_session_endpoint: 'https://dayebensa.auth.us-east-2.amazoncognito.com/logout'
    }*/

}