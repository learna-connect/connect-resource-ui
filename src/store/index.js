import Vue from 'vue'
import Vuex from 'vuex'
import { vuexOidcCreateStoreModule } from 'vuex-oidc'
import {oidcSettings} from "../config/oidc";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    oidcStore: vuexOidcCreateStoreModule(
        oidcSettings,

        {
          namespaced: true,
          dispatchEventsOnWindow: true
        },
        // Optional OIDC event listeners
        {
          userLoaded: (user) => Vue.$log.info('OIDC user is loaded:', user),
          userUnloaded: () => Vue.$log.info('OIDC user is unloaded'),
          accessTokenExpiring: () => Vue.$log.info('Access token will expire'),
          accessTokenExpired: () => Vue.$log.info('Access token did expire'),
          silentRenewError: () => Vue.$log.info('OIDC user is unloaded'),
          userSignedOut: () => Vue.$log.info('OIDC user is signed out'),
          oidcError: (payload) => Vue.$log.info('OIDC error', payload)
        }
    )
  }
})
