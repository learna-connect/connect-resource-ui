import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import UnderConstruction from "../views/UnderConstruction";
import ProfileView from "../views/ProfileView";
import ScheduleView from "../views/ScheduleView";
import OidcCallback from "../views/OidcCallback";
import store from '@/store'
import {vuexOidcCreateRouterMiddleware} from 'vuex-oidc'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            isPublic: true
        }
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/messages',
        name: 'Messages',
        component: UnderConstruction
    },
    {
        path: '/appointments',
        name:'Appointments',
        component: UnderConstruction
    },
    {
        path: '/profile',
        name:'Profile',
        component: ProfileView
    },
    {
        path: '/schedule',
        name:'Schedule',
        component: ScheduleView
    },
    {
        path: '/account',
        name:'Account',
        component: UnderConstruction
    },
    {
        path: '/user-management',
        name: 'User Management',
        component: UnderConstruction
    },
    {
        path: '/oidc-callback', // Needs to match redirectUri (redirect_uri if you use snake case) in you oidcSettings
        name: 'OidcCallback',
        component: OidcCallback
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach(vuexOidcCreateRouterMiddleware(store, 'oidcStore'));
export default router
